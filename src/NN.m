function res = NN (train, test)
  res = [];
  [N, p] = size(test);
  for i = 1:N
    [class, ref] = itNN(train, test(i, :));
    correct = (class == ref) * 1;
    res = [res; class, ref, correct];
  endfor
  
  
endfunction


function [class, ref] = itNN (train, new_obj)
  [N, p] = size(train);
  
  % Vetor de distancias
  D = [];
  
  for i = 1:N
    D = [D; norm(train(i,2:end) - new_obj(:, 2:end), 2)];
  endfor
 
  [nv, nid] = min(D);
  
  % Verdade terrestre
  ref = new_obj(1, 1);
  
  % Classe estimada
  class = train(nid, 1);
endfunction
