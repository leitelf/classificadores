function [res] = CQ (train, test)
  res = [];
  [N, p] = size(test);
  
  % Econtra centroides e matrizes de covariancia (agregada)
  [Ntrain, ptrain] = size(train);
  K = [];
  C = [];
  Cpool = zeros(ptrain-1, ptrain-1);

  for i = 1:max(train(:, 1));
    A = get_class(train, i);
    [Nclass, pclass] = size(A);
    K = [K; mean(A(:, :))];
    C = covar_69(A(:, 2:end));
    Cpool = Cpool + ( (1/(Nclass - 1)) * (C/(Nclass - 1)));
  endfor
  
  for i = 1:N
    [class, ref] = itCQ(K, Cpool, test(i, :));
    correct = (class == ref) * 1;
    res = [res; class, ref, correct];
  endfor
  
endfunction

function [class, ref] = itCQ (K, Cpool, new_obj)
  x = new_obj(:, 2:end);

  % Procura cluster mais proximo
  D = [];
  [N, p] = size(K);
  for i = 1:N
    aux = (x - K(i, 2:end));
    Cpoolinv = inv(Cpool);
    D = [D; aux * Cpoolinv * (aux)'];
  endfor
  [nv, nid] = min(D);
  
  % Verdade terrestre
  ref = new_obj(1, 1);
  
  % Classe estimada
  class = K(nid, 1);
endfunction

function [A] = get_class(X, class)
  [N, p] = size(X);
  A = [];
  for i = 1:N
    if (X(i, 1) == class)
      A = [A; X(i, :)];
    endif
  endfor
endfunction
