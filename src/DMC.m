function [res] = DMC(train, test)
  res = [];
  [N, p] = size (test);
  
  % Econtra centroides
  K = [];
  for i = 1:max(train(:, 1))
    A = get_class(train, i);
    K = [K; mean(A(:, :))];
  endfor
  
  for i = 1:N
    [class, ref] = itDMC(K, test(i, :));
    correct = (class == ref) * 1;
    res = [res; class, ref, correct];
  endfor
endfunction

function [class, ref] = itDMC (K, new_obj)
  % Procura cluster mais proximo
  D = [];
  [N, p] = size(K);
  for i = 1:N
    D = [D; norm(K(i, 2:end) - new_obj(:, 2:end), 2)];
  endfor
  [nv, nid] = min(D);
  
  % Verdade terrestre
  ref = new_obj(1, 1);
  
  % Classe estimada
  class = K(nid, 1);
endfunction

function [A] = get_class(X, class)
  [N, p] = size(X);
  A = [];
  for i = 1:N
    if (X(i, 1) == class)
      A = [A; X(i, :)];
    endif
  endfor
endfunction