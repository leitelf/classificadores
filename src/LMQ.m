function [res] = LMQ(train, test)
  res = [];
  [N, p] = size(test);
  Nk = max(train(:, 1));
  phi = get_coefs(train);
  
  for i = 1:N
    [class, ref] = itLMQ(test(i, :), phi, Nk);
    correct = (class == ref) * 1;
    res = [res; class, ref, correct];
  endfor
  
endfunction

function [phi] = get_coefs(train)
  
  for i = 1:max(train(:, 1))
    X = get_class(train, i);
    X = X(:, 2:end);
    Y = ones(size(X, 1), 1);
    
    Xc = get_complement(train, i);
    Xc = Xc(:, 2:end);
    X = [X; Xc];
    Y = [Y; zeros(size(Xc, 1), 1)];
    
    phi{i} = pinv(X' * X) * X' * Y;
  endfor
endfunction

function [class, ref] = itLMQ (new_obj, phi, Nk)
  
  % Vetor de valores de discriminante
  D = [];
  x = new_obj(:, 2:end);
  
  for i = 1:Nk
    D = [D; x * phi{i}];
  endfor
  
  #idx = round(new_obj * phi);
  idx = get_closest(D, 1);
  
  % Verdade terrestre
  ref = new_obj(1, 1);
  
  % Classe estimada
  class = idx;
endfunction

function [idx] = get_closest(X, val)
  N = size(X, 1);
  D = [];
  for i = 1:N
    D = [D; abs(val - X(i))];
  endfor
  [minVal, idx] = min(D);
endfunction

function [A] = get_complement(X, class)
  [N, p] = size(X);
  A = [];
  for i = 1:N
    if (X(i, 1) != class)
      A = [A; X(i, :)];
    endif
  endfor
endfunction

function [A] = get_class(X, class)
  [N, p] = size(X);
  A = [];
  for i = 1:N
    if (X(i, 1) == class)
      A = [A; X(i, :)];
    endif
  endfor
endfunction
